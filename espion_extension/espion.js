///////////////////////////FONCTIONS/////////////////////////////////

function handleResponse(message) {
  console.log(`${message.response}`);
}

function handleError(error) {
  console.log(`Error: ${error}`);
}

function notifyBackgroundPage(e) {
  var sending = browser.runtime.sendMessage({
    test: e
  });
  sending.then(handleResponse, handleError);
}

/////////////////////SCRIPT///////////////////////////////////////////

console.log('coucou');

var liens = [];
var txt;
var count = 0;

var observer = new MutationObserver(function(mutations) {

  // For the sake of...observation...let's output the mutation to console to see how this all works
	liens.push(window.location.href);
  console.log('yes');

  if (count == 4) {
    notifyBackgroundPage(liens);
    liens = [];
    console.log(liens);
  }

  count = (count+1)%5;
  console.log(count);

});

// Notify me of everything!
var observerConfig = {
	childList: true,
	characterData: true,
};

// Node, config
var targetNode = document.getElementById('movie_player');

observer.observe(targetNode, observerConfig);
