// jshint esversion: 8

////////////////////// VARIABLES //////////////////////
var oldLogLast;
var requestURL = 'http://curlybraces.be/erg/2019-2020/portrait-robot/log_last.json';

////////////////////// FONCTIONS //////////////////////
////récupérer le dernier log, et si il est nouveau mettre à jour la page
function getLastLog() {
  var request = new XMLHttpRequest();
  request.open('GET', requestURL);
  request.responseType = 'json';
  request.send();
  request.onload = function() {
    if (JSON.stringify(oldLogLast) != JSON.stringify(request.response)) {
      ////vérifier que la nouvelle vidéo trouvée est bien différente de la précédente
      var oldVideoId = document.getElementsByTagName('iframe')[0].src.replace(/https:\/\/www\.youtube-nocookie\.com\/embed\/(.*?)\?.*/, '$1');
      // console.log('old', oldVideoId);
      // console.log('new', request.response.id);
      if (request.response.id != oldVideoId) {
        changeVideo(request.response.id);
        newPost(request.response);
        newImage(request.response);
      }
    }
    oldLogLast = request.response;
  };
}

////remplacer l'ancienne vidéo par la nouvelle
function changeVideo(id) {
  console.log('nouvelle vidéo');
  let originalSrc = document.getElementsByTagName('iframe')[0].src;
  let newSrc = originalSrc.replace(/(https:\/\/www\.youtube-nocookie\.com\/embed\/).*?(\?).*?&/, '$1' + id + '$2');
  document.getElementsByTagName('iframe')[0].src = newSrc;
}

////rajouter un post dans le corps de texte du site + une date
function newPost(obj) {
  let sectionTexte = document.querySelector('section#texte');
  let htmlPost = `
  <aside class="date">${obj.datetime.jour}</br>${obj.datetime.heure}</aside>
  <div class="inline">
  <span class="span"> Je regarde une nouvelle vidéo. Le titre est <a href="https://www.youtube.com/watch?v=${obj.id}" class="titre" target="_blank">${obj.snippet.title}</a>. Elle dure <span class="durée">${obj.contentDetails.duration}</span>. </span>
  </div>
  `;
  sectionTexte.insertAdjacentHTML('beforeend', htmlPost);
}

////rajouter une image dans la partie images
function newImage(obj) {
  let sectionImages= document.querySelector('section#images');
  let htmlImage = `<div><img src="${obj.snippet.thumbnailURL}"></div>`;
  sectionImages.insertAdjacentHTML('beforeend', htmlImage);
}

////////////////////// PROGRAMME //////////////////////
var yes = setInterval(getLastLog, 3000);
