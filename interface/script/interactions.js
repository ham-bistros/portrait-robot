// jshint esversion: 8
//////////////////// VARIABLES ////////////////////
var imgs = document.getElementsByTagName('img');

//////////////////// FONCTIONS ////////////////////
function clamp(value, min, max) {
  return Math.min(Math.max(value, min), max);
}

//////////////////// SCROLL IMAGES ////////////////////
var number = 0;
var prev_number = 0;
var coeff = 1/2;

document.getElementById('images').addEventListener('wheel', function(event) {
  if (event.deltaY < 0) {
    //scrolling up
    prev_number = number;
    number = clamp(number - coeff, 0, imgs.length - 1);
  } else if (event.deltaY > 0) {
    //scrolling down
    prev_number = number;
    number = clamp(number + coeff, 0, imgs.length - 1);
  }
  imgs[Math.round(number)].parentElement.style.display = 'inherit';
  if (Math.round(number) != Math.round(prev_number)) {
    imgs[Math.round(prev_number)].parentElement.style.display = 'none';
  }
});

/////////////////////// TEST DE SPLIT ///////////////////////
Split(['#live', '#texte', '#images'], {
  sizes: [30, 50, 20],
  minSize: [200, 200, 200],
  gutterAlign: 'center',
  gutterSize: 2,
});
