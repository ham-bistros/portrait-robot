<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style2.css" type="text/css">
  <title>PORTRAIT-ROBOT</title>
  <?php
  ////////////////////// AFFICHER LES ERREURS ///////////////////////
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    ini_set('max_execution_time', 300);

  //////////////////////////// FONCTIONS ////////////////////////////
    function debug_to_console($data)
    {
        $output = $data;
        if (is_array($output)) {
            $output = implode(',', $output);
        }
        echo "<script>console.log('".$output."' );</script>";
    }

    function nb_seconds($str)
    {
        $h_m_s = explode(':', $str);
        if (count($h_m_s) == 1) {
            ////secondes
            return $h_m_s[0];
        } elseif (count($h_m_s) == 2) {
            ////minutes et secondes
            return $h_m_s[0]*60 + $h_m_s[1];
        } elseif (count($h_m_s) == 3) {
            ////heures, minutes et secondes
            return $h_m_s[0]*3600 + $h_m_s[1]*60 + $h_m_s[2];
        }
    }

    function moyenne_array($arr)
    {
        $somme = 0;
        $i = 0;
        foreach ($arr as $val) {
            $i++;
            $somme += $val;
        }
        return round($somme/$i);
    }

  /////////////////////////// VARIABLES /////////////////////////////
    $arr_JSON = [];
    $compare_tags = [];
    $compare_durée = [];
    $compare_catégories = [];
    $compteur = 0;
    ////array pour trouver l'image de meilleure qualité
    $thumb_res = ['maxres', 'high', 'medium', 'default'];
    ////array avec un choix de morceaux de phrases pour dire la même chose
    $jaime_bien = ['J\'aime bien ', 'Je suis à fond dans ', 'J\'adore ', 'Je préfère ', 'Je suis plutôt dans ', 'J\'aime assez ', 'J\'apprécie ', 'J\'aime beaucoup '];


  /////////////////////////RÉCUPÉRER LE CONTENU DES LOGS/////////////////////////
    $JSON_count = 0;
    $fh = fopen('log_results.json', 'r');
    while ($line = fgets($fh)) {
        array_push($arr_JSON, json_decode($line, true));
        $JSON_count += 1;
    }
    fclose($fh);

  /////////////////////////RÉCUPÉRER LE DERNIER LOG/////////////////////////
    $log_last = $arr_JSON[count($arr_JSON)-1];

  /////////////////////////DÉBUT DE LA VIDÉO/////////////////////////
    ////permet d'avoir un timestamp du moment où la vidéo a été lancée sur le RPi (environ)
    $now = strtotime($arr_JSON[count($arr_JSON)-1]['datetime']['now']);
    $time_diff = time() - $now;
  ?>

</head>

<body>
  <?php
  ////////////////////// GÉRER LA REQUÊTE POST ///////////////////////
    if (isset($_POST)) {
        $json = file_get_contents('php://input');
        if (!empty($json)) {
            ////on met le dernier log dans un fichier à part pour que la requête AJAX n'ait pas trop de trucs à charger (surtout quand le fichier sera bien rempli)
            file_put_contents('log_last.json', $json);
            ////on utilise PHP_EOL (End Of Line) pour créer un saut à la ligne à la fin du fichier
            file_put_contents('log_results.json', $json.PHP_EOL, FILE_APPEND);
        } else {
            debug_to_console('input POST vide');
        }
    } else {
        debug_to_console('pas de variable $_POST');
    }
  ?>
  <header>
    <a href="about.html" id="about-header" class="header-a">À PROPOS</a><a href="https://pratiquesnumeriques.be/index.php?title=Utilisateur:Thomas#PORTRAIT-ROBOT" target="_blank" id="wiki-header" class="header-a">WIKI</a><a href="reste.html" id="reste-header" class="header-a">RESTE DU PROJET</a><a href="https://gitlab.com/123450/portrait-robot" target="_blank" id="gitlab-header" class="header-a">CODE</a>
 </header>

  <section id="live" class="split">

    <div class="wrapper">
      <?php

        echo '<iframe id="iframe" src="https://www.youtube-nocookie.com/embed/'.$log_last['id'].'?start='.$time_diff.'&autoplay=1&modestbranding=1&showinfo=0&rel=0&iv_load_policy=3&theme=light&color=white&controls=0&frameborder=0&allowfullscreen=true" name="vidéo"></iframe>';
      ?>
    </div>

  </section>

  <section id="texte" class="split">
    <?php
      ////afficher le contenu des logs dans la partie archive
      foreach ($arr_JSON as $JSON) {
          echo '<article class="post">';
          ////afficher la date
          echo '<aside class="date">'.$JSON['datetime']['jour'].'</br>'.$JSON['datetime']['heure'].'</aside>';
          echo '<div class="inline">';
          echo '<span class="span">';
          echo 'Je regarde une nouvelle vidéo. Le titre est <a href="https://www.youtube.com/watch?v='.$JSON['id'].'" class="titre" target="_blank">'.$JSON['snippet']['title'].'</a>.
            Elle dure <span class="durée">'.$JSON['contentDetails']['duration'].'. </span> ';
          echo '</span>';

          ////comparer les tags des dernières vidéos
          ////s'il y a des tags, on les range dans une array pour les comparer
          if (isset($JSON['snippet']['tags'])) {
              foreach ($JSON['snippet']['tags'] as $tag) {
                  array_push($compare_tags, $tag);
              }
          }
          if ($compteur%4 == 3) {
              $compare_tags = array_map('strtolower', $compare_tags);
              $compare_tags = array_count_values($compare_tags);
              arsort($compare_tags);
              $sorted_tags = array_keys($compare_tags);
              $sorted_keys = array_keys(array_flip($compare_tags));
              ////affiche le tag le plus récurrent si il y a plus d'une occurence (et donc si c'est un minimum pertinent)
              if ($sorted_keys[0] > 1) {
                  echo '<span class="span">';
                  $phrase = strtolower($jaime_bien[mt_rand(0, count($jaime_bien)-1)]);
                  echo 'En ce moment '.$phrase.' '.$sorted_tags[0].' ! ';
                  echo '</span>';
                  ////vide l'array, permet de comparer seulement les x derniers éléments
                  $compare_tags = [];
              }
          }

          ////comparer les durées des vidéos
          if (isset($JSON['contentDetails']['duration'])) {
              array_push($compare_durée, $JSON['contentDetails']['duration']);
          }
          if ($compteur%2 == 1) {
              $compare_durée = array_map('nb_seconds', $compare_durée);
              $moy_dur =  moyenne_array($compare_durée);
              echo '<span class="span">';
              ////commentaire en fonction de la durée moyenne des vidéos
              if ($moy_dur <= 120) {
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].' les vidéos très courtes. ';
              } elseif ($moy_dur <= 600) {
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].' les vidéos courtes. ';
              } elseif ($moy_dur <= 1200) {
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].' les vidéos pas trop longues. ';
              } elseif ($moy_dur <= 2400) {
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].' les vidéos plutôt longues. ';
              } elseif ($moy_dur <= 5000) {
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].' les vidéos longues. ';
              } elseif ($moy_dur <= 18000) {
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].'les vidéos vraiment longues. ';
              }
              echo '</span>';
              ////vide l'array, permet de comparer seulement les x derniers éléments
              $compare_durée = [];
          }

          ////comparer les catégories
          ////s'il y a une catégorie, on les range dans une array pour les comparer
          if (isset($JSON['snippet']['categoryId'])) {
              array_push($compare_catégories, $JSON['snippet']['categoryId']);
          }
          if ($compteur%4 == 3) {
              ////compare les tags des dernières vidéos et en sort le plus récurrent
              $compare_catégories = array_map('strtolower', $compare_catégories);
              $compare_catégories = array_count_values($compare_catégories);
              arsort($compare_catégories);
              $sorted_catégories = array_keys($compare_catégories);
              $sorted_cat_keys = array_keys(array_flip($compare_catégories));
              ////affiche le tag le plus récurrent si il y a plus d'une occurence (et donc si c'est un minimum pertinent)
              if ($sorted_cat_keys[0] > 1) {
                  echo '<span class="span">';
                  echo $jaime_bien[mt_rand(0, count($jaime_bien)-1)].' les vidéos de '.$sorted_catégories[0].'.';
                  echo '</span>';
                  ////vide l'array, permet de comparer seulement les x derniers éléments
                  $compare_catégories = [];
              }
          }
          // debug_to_console($compteur%4);
          $compteur += 1;
          echo '</div>';
          echo '</article>';
      }
    ?>
  </section>

  <section id="images" class="split">
    <?php
    ////on affiche les images dans une section différente donc on refait une boucle
      foreach ($arr_JSON as $images_JSON) {
          echo '<div>
                <img src="'.$images_JSON['snippet']['thumbnailURL'].'">
                </div>';
      }
    ?>
  </section>

  <!-- SCRIPTS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/split.js/1.5.11/split.min.js"></script>
  <script src="script/interactions.js" type="text/javascript"></script>
  <script src="script/liveLog.js" type="text/javascript"></script>
</body>
</html>
