#!/usr/bin/env python3
# coding: utf-8

"""Programme qui va sur YouTube."""


########################IMPORTS########################
###SELENIUM
from time import sleep
import pickle
import pytz
from re import sub
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import selenium.common.exceptions

###API YOUTUBE
from babel.dates import format_date
from json import loads as json_loads
from datetime import datetime
import googleapiclient.discovery
import googleapiclient.errors

###pour pouvoir lancer le programme depuis supervisor
base_dir = '/home/tumtum/Documents/BOULOT/AP/portrait-robot/'
###pour le Raspberry Pi
# base_dir = '/home/pi/Documents/portrait-robot/'

#####################RÉCUPÉRER TITRE + URL DE LA VIDÉO#####################
def get_infos():
    titre = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#container > h1 > yt-formatted-string")))
    print('  Le titre de la vidéo est "%s".' % titre.get_attribute("textContent")) #NARRATIF
    ###afficher l'url actuelle
    print('    L\'url est [%s].' % browser.current_url)  #NARRATIF
    ###l'enregistrer dans le log (si ce n'est pas la même URL que la précédente vidéo)
    with open(base_dir+'log_urls.txt', 'r', encoding='utf8') as f_2:
        last_vids = f_2.read()
        last_vids = last_vids.splitlines()
    with open(base_dir+'log_urls.txt', 'a', encoding='utf8') as f_2:
        ###si l'array last_vids n'est pas vide
        if last_vids:
            ###si la dernière URL dans le log est différente de l'url actuelle
            if last_vids[len(last_vids)-1] != browser.current_url:
                f_2.write(browser.current_url + '\n')
                API_call(browser.current_url)
        else:
            f_2.write(browser.current_url + '\n')
            API_call(browser.current_url)

######################REPÉRER LE CHANGEMENT DE PAGE######################
def new_vid():
    """Fonction qui lance un protocole à chaque changement de page."""
    get_infos()
    upnext = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#dismissable > div > div.metadata.style-scope.ytd-compact-video-renderer > a")))
    # elem = browser.find_element_by_css_selector('#dismissable > div > div.metadata.style-scope.ytd-compact-video-renderer > a')
    up_link = upnext.get_attribute("href")
    print('      Je regarde la vidéo jusqu\'au bout.')
    ###attendre jusqu'à 5h max que la prochaine vidéo arrive (il y a peu de chance qu'il y ait des vidéos de + de 5h)
    WebDriverWait(browser, 18000).until(EC.url_to_be(up_link))
    print('\nUne nouvelle vidéo !')  #NARRATIF
    print(datetime.now())
    sleep(1)

###############COMPTER LES LETTRES EN CAPITALE###############
def nbr_uppr(s):
    uppercase = 0
    for c in s:
        if c.isupper():
            uppercase += 1
        else:
            pass
    proportion = uppercase/len(s)
    return proportion

##########################APPEL À L'API###########################
#authentification et configuration de l'API
api_service_name = "youtube"
api_version = "v3"
max_results = 1

with open(base_dir+'API.txt', 'r', encoding="utf-8") as f:
    DEVELOPER_KEY = f.read().rstrip()

youtube = googleapiclient.discovery.build(
    api_service_name, api_version, developerKey=DEVELOPER_KEY)

def API_call(url):
    ###récupérer seulement l'id de la vidéo
    vid_id = url.replace('https://www.youtube.com/watch?v=', '')
    print(vid_id, '-> l\'id de la vidéo')
    ###la requête à l'API en elle-même
    request = youtube.videos().list(
        part="snippet,contentDetails,statistics",
        id=vid_id,
    )
    try:
        response = request.execute()['items'][0]

        ###supprimer les éléments inutiles pour alléger le fichier au maximum
        supp = ['kind', 'etag']
        for s in supp:
            del response[s]

        supp2 = ['channelId', 'liveBroadcastContent', 'localized']
        for s2 in supp2:
            if s2 in response['snippet']:
                del response['snippet'][s2]

        supp3 = ['dimension', 'projection', 'contentRating', 'licensedContent', 'regionRestriction']
        for s3 in supp3:
            if s3 in response['contentDetails']:
                del response['contentDetails'][s3]

        ###trouver le thumbnail avec les plus grande définition pour chaque url
        thumb_sizes = ['maxres', 'high', 'medium', 'standard', 'default']
        for thumb in thumb_sizes:
            if thumb in response['snippet']['thumbnails']:
                print(thumb)
                response['snippet']['thumbnailURL'] = response['snippet']['thumbnails'][thumb]['url']
                break
        del response['snippet']['thumbnails']

        ###rajouter la date et l'heure courante dans le JSON
        date = datetime.now(pytz.utc)
        jour = format_date(date, format='full', locale='fr_FR')
        jour = jour[:1].upper()+jour[1:].replace(' 2020', '')
        heure = date.strftime("%H:%M")
        ###on utilise  __str__ pour convertir la valeur de l'objet datetime en string
        ###l'objet datetime.datetime.now() n'est pas sérialisable (on ne peut pas le mettre dans du JSON)
        date_time = {"jour":jour, "heure":heure, "now":date.__str__()}
        response['datetime'] = date_time

        ###changer le categoryId par une chaîne de caractère compréhensible
        with open(base_dir+'catégoriesVidéos.json', 'r', encoding='utf-8') as f2:
            data = f2.read()
            video_categories = json_loads(data)['items']
            for cat in video_categories:
                if response['snippet']['categoryId'] == cat['id']:
                    response['snippet']['categoryId'] = cat['title']

        ###simplifier un peu le format de la durée de la vidéo
        durée_simple = response['contentDetails']['duration'].lstrip('PT').lower()
        durée_simple = sub('([hm])', r'\1 ', durée_simple)
        durée_simple = sub(r' (\d[ms])', r' 0\1', durée_simple)
        ###date au format HH:MM:SS
        durée_simple = durée_simple.replace(' ', '')
        durée_simple = sub('[hm]', ':', durée_simple).rstrip('s')
        response['contentDetails']['duration'] = durée_simple

        ###envoyer le tout sur le serveur
        url = 'http://curlybraces.be/erg/2019-2020/portrait-robot/index.php'
        req = requests.post(url, json=response)
        print(req)
        print(req.json)
    except IndexError:
        print('Je ne trouve pas d\'infos sur la vidéo.')

####################EXÉCUTION SELENIUM####################
options = webdriver.ChromeOptions()
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_argument("--mute-audio")
options.add_argument("--disable-extensions")
options.add_argument("--disable-gpu")
options.add_argument("--no-sandbox")
options.add_experimental_option("excludeSwitches", ['enable-automation'])
options.add_experimental_option('useAutomationExtension', False)
options.add_argument("headless")
browser = webdriver.Chrome(options=options)
browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
    "source": """
    Object.defineProperty(navigator, 'webdriver', {
      get: () => undefined
    })
  """
})
browser.execute_cdp_cmd("Network.enable", {})
wait = WebDriverWait(browser, 200)

print('J\'allume Internet.') #NARRATIF
browser.get('https://www.youtube.com/')
print('  Je vais sur YouTube, comme d\'habitude.') #NARRATIF

######################IMPORTER LES COOKIES######################
cookies = pickle.load(open(base_dir+"cookie_YT/cookies_cinna.pkl", "rb"))
for i, cookie in enumerate(cookies):
    try:
        browser.add_cookie(cookie)
        # print('COOKIE')
    except NameError:
        print('Le cookie %s ne marche pas' % i)
sleep(2)
print('    Je me connecte automatiquement\n') #NARRATIF

###REFRESH
browser.refresh()
print('refresh')

##############LANCER LA DERNIÈRE VIDÉO ENREGISTRÉE DANS LE LOG###############
###choper la dernière url enregistrée dans le log pour recommencer depuis l'arrêt du programme
with open(base_dir+'log_urls.txt', 'r', encoding='utf8') as f:
    vids = f.read()
    if len(vids) > 0:
        vids = vids.splitlines()
        ###pour avoir le dernier élément de la liste
        vid = vids[len(vids)-1]
        ###lancer la dernière vidéo du log
        browser.get(vid)
        print('Je regarde la vidéo de la dernière fois.') #NARRATIF
    else:
        ###cliquer sur la vidéo avec le plus de lettre en CAPITALE dans le titre ?
        titres = browser.find_elements_by_id('video-title')
        proportion_upper = []
        for r in range(0,8):
            str_titre = str(titres[r].get_attribute('textContent'))
            proportion_upper.append(nbr_uppr(str_titre))
        max_index = proportion_upper.index(max(proportion_upper))
        titres[max_index].click()
        print('Je regarde la vidéo qui m\'inspire le plus.') #NARRATIF

######################LANCER LA VIDÉO######################
###cliquer sur le bouton play pour lancer la première vidéo
try:
    elem = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#movie_player > div.ytp-cued-thumbnail-overlay > button")))
    print('Je clique sur le bouton "play" pour lancer la vidéo.') #NARRATIF
    elem.click()
except selenium.common.exceptions.TimeoutException:
    print('pas de bouton à cliquer')

##########BOUCLE QUI SE RÉPÈTE À CHAQUE NOUVELLE VIDÉO##########
try:
    while True:
        new_vid()
except NameError:
    print('\nLe programme a planté :/', '\nNameError')
except KeyboardInterrupt:
    print('\nKeyboardInterrupt')
