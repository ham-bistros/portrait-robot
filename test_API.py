#!/usr/bin/env python3
# coding: utf-8

from babel.dates import format_date
import pytz
import json
import re
import requests
from datetime import datetime
import googleapiclient.discovery
import googleapiclient.errors

#authentification et configuration de l'API
api_service_name = "youtube"
api_version = "v3"
max_results = 1

url = 'https://www.youtube.com/watch?v=uXYC0GrHAvE'
# vid_id = url.lstrip('https://www.youtube.com/watch?v=')
vid_id = url.replace('https://www.youtube.com/watch?v=', '')

print(vid_id)

with open('API.txt', 'r', encoding="utf-8") as f:
    DEVELOPER_KEY = f.read().rstrip()

youtube = googleapiclient.discovery.build(
    api_service_name, api_version, developerKey=DEVELOPER_KEY)

###la requête à l'API en elle-même
request = youtube.videos().list(
    part="snippet,contentDetails,statistics",
    id=vid_id,
)

try:
    response = request.execute()['items'][0]

    ###supprimer les éléments inutiles pour alléger le fichier au maximum
    supp = ['kind', 'etag']
    for s in supp:
        del response[s]

    supp2 = ['channelId', 'liveBroadcastContent', 'localized']
    for s2 in supp2:
        if s2 in response['snippet']:
            del response['snippet'][s2]

    supp3 = ['dimension', 'projection', 'contentRating', 'licensedContent', 'regionRestriction']
    for s3 in supp3:
        if s3 in response['contentDetails']:
            del response['contentDetails'][s3]

    ###trouver le thumbnail avec les plus grande définition pour chaque url
    thumb_sizes = ['maxres', 'high', 'medium', 'standard', 'default']
    for thumb in thumb_sizes:
        if thumb in response['snippet']['thumbnails']:
            print(thumb)
            response['snippet']['thumbnailURL'] = response['snippet']['thumbnails'][thumb]['url']
            break
    del response['snippet']['thumbnails']

    ###rajouter la date et l'heure courante dans le JSON
    date = datetime.now(pytz.utc)
    jour = format_date(date, format='full', locale='fr_FR')
    jour = jour[:1].upper()+jour[1:].replace(' 2020', '')
    heure = date.strftime("%H:%M")
    ###on utilise  __str__ pour convertir la valeur de l'objet datetime en string
    ###l'objet datetime.datetime.now() n'est pas sérialisable (on ne peut pas le mettre dans du JSON)
    date_time = {"jour":jour, "heure":heure, "now":date.__str__()}
    response['datetime'] = date_time

    ###changer le categoryId par une chaîne de caractère compréhensible
    with open('catégoriesVidéos.json', 'r', encoding='utf-8') as f:
        data = f.read()
        video_categories = json.loads(data)['items']
        for cat in video_categories:
            if response['snippet']['categoryId'] == cat['id']:
                response['snippet']['categoryId'] = cat['title']

    ###changer le format de la durée de la vidéo
    durée_simple = response['contentDetails']['duration'].lstrip('PT').lower()
    durée_simple = re.sub('([hm])', r'\1 ', durée_simple)
    durée_simple = re.sub(r' (\d[ms])', r' 0\1', durée_simple)
    durée_simple = durée_simple.replace(' ', '')
    durée_simple = re.sub('[hm]', ':', durée_simple).rstrip('s')
    print(durée_simple)
    response['contentDetails']['duration'] = durée_simple

    ###enregistrer le tout dans un fichier JSON
    with open('result_API.json', 'w', encoding="utf8") as fp:
        json.dump(response, fp)
        fp.write('\n')

    ###envoyer le tout sur le serveur
    # url = 'http://curlybraces.be/erg/2019-2020/portrait-robot/index.php'
    # r = requests.post(url, json=response)
    # print(r)
    # print(r.json)

except IndexError:
    print('Je ne trouve pas d\'info sur cette vidéo')
